import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../../services/validate.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name:String;
  username:String;
  email:String;
  password:String;

  constructor(
    private validateService:ValidateService,
    private flashmessage:FlashMessagesService,
    private authService:AuthService,
    private router:Router
    ) { }

  ngOnInit() {
  }
  onRegisterSubmit(){
    console.log(this.name);
    const user={
      name:this.name,
      username:this.username,
      email:this.email,
      password:this.password
    }
    //Req fields
    if(!this.validateService.validateRegister(user)){
      this.flashmessage.show('Please fill all fields',{cssClass:'alert-danger',timeout:3000});
      return false;
    }
    //val email
    if(!this.validateService.validateEmail(user.email)){
      this.flashmessage.show('Please enter valid Email',{cssClass:'alert-danger',timeout:3000});
      return false;
    }
    
    //register user
    this.authService.registerUser(user).subscribe(data=>{
      if(data.success){
        this.flashmessage.show('Registered Successfully',{cssClass:'alert-danger',timeout:3000});
        this.router.navigate(['/login']);
      }else{
        this.flashmessage.show('Registration Failed',{cssClass:'alert-danger',timeout:3000});
        this.router.navigate(['/register']);
      }
    });
  }
}
